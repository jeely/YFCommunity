
<div align="center" >
    <img src="https://foruda.gitee.com/avatar/1728719990621654075/13695585_qingyun-software_1728719990.png!avatar100" />
</div>

<div align="center">

智慧小区智慧社区智慧物业管理系统Java版本

</div>

<div align="center" >
    <a href="https://www.sdyingfeng.cn">
        <img src="https://img.shields.io/badge/Licence-apache2.0-green.svg?style=flat" />
    </a>
    <a href="https://www.sdyingfeng.cn">
        <img src="https://img.shields.io/badge/Edition-5.4-blue.svg" />
    </a>
     <a href="https://gitee.com/ZhongBangKeJi/CRMEB/repository/archive/master.zip">
        <img src="https://img.shields.io/badge/Download-240m-red.svg" />
    </a>
</div>

#### 

<div align="center">

[官网](https://sdyingfeng.cn/?shequ) |
[在线体验](http://113.128.179.250:9991/) |
[宽屏预览](https://gitee.com/qingyun-software/YFCommunity/edit/master/README.md) 

</div>

<div align="center">
    如果对您有帮助，您可以点右上角 "Star" ❤️ 支持一下 谢谢！
</div>

---

### 📖 简介：

🔥萤丰-开源智慧小区智慧社区系统Java版，为打造未来居住体验，智能家居，科技引领未来，让您的生活更美好追求高科技智能生活，采用智能家居设计，为您提供更加便捷、舒适的居住体验。从安全系统到智能控制系统，让您的生活一步到位，享受科技带来的便捷和快乐！</br> 
🔥基于微服务架构+Java+Spring Cloud +Uni-App +MySql开发，在PC、APP、小程序、H5、移动端都能使用，同时支持独立部署，二开，商用，自用等形式。


💻 萤丰开源智慧小区智慧社区系统Java版本：
>一、智慧运营平台：http://113.128.179.250:9990/</br> 
>二、智慧物管平台：http://113.128.179.250:9991/ </br>
>三、小区治理平台 ：http://113.128.179.250:9992/ </br>
>四、物联网平台：http://113.128.179.250:9993/ </br>
>五、物业h5 ：http://113.128.179.250:9994/ </br>
>六、业主h5：http://113.128.179.250:9981/ </br>
---


📌 系统代码全开源无加密，独立部署、二开方便，可针对不同小区，不同物业，创建多套小区物业系统，同时支持一个物业对应多个小区管理，方便物业管理，同时包含八大端口和两个数字驾驶舱，有“业主移动端，物业移动端，智慧物管平台，小区治理平台，智慧运营平台，智慧物联网一体化平台，智慧社区数字中台，智慧社区开放平台，智慧物业智能驾驶舱，智慧小区运营智能驾驶舱”端口功能。

---

### 💡 系统亮点：
>1.SpringBoot 框架开发业界主流。  </br>
>2.【前端】Web PC 管理端 Vue + Element UI。<br>
>3.【前端】移动端使用 Uni-app 框架，前后端分离开发。<br>
>4.标准RESTful 接口、标准数据传输，逻辑层次更明确，更多的提高api复用。<br>
>5.支持Redis队列，降低流量高峰，解除耦合，高可用。<br>
>6.数据导出，方便个性化分析。<br>
>7.数据统计分析,使用ECharts图表统计，实现多种统计分析。<br>
>8.Spring Security 权限管理，后台多种角色，多重身份权限管理，权限可以控制到按钮级别的操作。<br>
>9.Vue表单生成控件，拖拽配置表单，减少前端重复表单工作量，提高前端开发效率。<br>
>9.Uniapp生成小程序、H5、移动端都能使用<br>
---

### 💻 运行环境及框架：
~~~
1.	WEB Pc 管理后台使用Vue + Element UI 开发 兼容主流浏览器 ie11+
2.	后台服务 Java SpringBoot + MybatisPlus + Mysql + redis + nacos
3.	运行环境 linux和windows等都支持,只要有Java环境和对应的数据库 redis
4.	运行条件 Java 1.8 Mysql8.4.0
~~~
---

### 🔧 Java项目框架 和 WEB PC 项目运行环境
~~~
1. SpringBoot 2.7.1
2. Jdk 1.8
3. Maven 3.5.0+   
4. Mysql 8.4.0
5. Redis (版本不限)
6. Nacos（2.2.0)
7. nginx 1.24.0
8. npm 8.19.1
9. node v14.18.0
10. vue 2.x
11. element ui 2.13
~~~

---

### 🧭 项目代码包介绍
~~~
1. admin     WEB程序         Java SpringCloud + mybatisPlus
   ├── wr-gateway         // 网关模块 
   ├── wr-auth            // 认证中心 
   ├── wr-modules         // 业务模块
   │       └── modules-job                            // 定时任务 
   │       └── modules-file                           // 文件服务 
   │       └── modules-system                         // 智慧运营平台 
   │       └── modules-estate                         // 智慧物管平台 
   │       └── modules-aiot                           // 物联网应用一体化平台 
   │       └── modules-govern                         // 小区治理平台 
   │       └── modules-payment                        // 支付模块 
   │       └── modules-work                           // 物业工单模块 
   ├── wr-api             // 接口模块 
   │       └── api-base                               // 基础接口模块 ---此模块不写接口
   │       └── api-device                             // 硬件设备接口模块 
   │       └── api-estate                             // 物业小程序接口模块 
   │       └── api-owner                              // 业主H5接口模块 
   │       └── api-payment                            // 停车缴费接口模块 
   ├── wr-common          // 通用工具模块
   │       └── common-core                            // 核心模块
   │       └── common-datascope                       // 权限范围
   │       └── common-datasource                      // 多数据源
   │       └── common-log                             // 日志记录
   │       └── common-redis                           // 缓存服务
   │       └── common-security                        // 安全模块
   │       └── common-swagger                         // 系统接口
   │       └── common-customize                       // 自定义模块，所有自定义的类放到此模块中
   ├── wr-remote          // 模块间接口调用模块
   └── wr-visual          // 图形化管理模块
   
2. front     PC端管理端       VUE + ElementUi
   ├── admin-ui           // 通用端ui 
   ├── estate-ui          // 物业端ui 
   ├── aiot-ui            // AIoT端ui 
   └── govern-ui          // 社区治理ui 
3. 接口文档   Api对应的接口文档也可以部署项目后查看
~~~

---

### 🎬 系统演示：


1、💻 PC端演示：  <br>

请联系客服获取,客服微信：tlywlf<br>

(1)智慧运营平台：
http://113.128.179.250:9990/ 
~~~
账号：system 密码：联系客服
~~~
(2)智慧物管平台：
http://113.128.179.250:9991/ 
~~~
账号：18888888888 密码：联系客服
~~~
(3)小区治理平台：
http://113.128.179.250:9992/ 
~~~
账号：15788888888 密码：联系客服
~~~
(4)物联网平台：
 http://113.128.179.250:9993/ 
~~~
账号：18888888888 密码：联系客服
~~~

2、🖥️ 大屏端演示：<br>
(1)智慧小区运营智能驾驶舱：
 http://113.128.179.250:9990/screen
~~~
账号：system 密码：联系客服
~~~
(2)智慧物业智能驾驶舱
http://113.128.179.250:9991/screen
~~~
账号：18888888888 密码：联系客服
~~~


3、📱 移动端演示：
~~~
 物业h5地址:http://113.128.179.250:9994/ 
 账号：18888888888  密码：联系客服

 业主h5地址:http://113.128.179.250:9981/ 
 账号：16555384888 密码：联系客服
~~~

---

###  📞 联系我们

如果您有任何疑问或需要咨询，欢迎与我们联系：
~~~
客服微信：lhl-bmy-bhw,tlywlf
吕经理： 13241588504（微信同号）
牛经理： 15854827610（微信同号）
~~~

---

###  🚩 产品图

![输入图片说明](readme/xiaoqu1.jpg)
![输入图片说明](readme/xiaoqu2.png)
![输入图片说明](readme/xiaoqu3.png)
![输入图片说明](readme/xiaoqu4.png)
---

###  📺 架构图

![输入图片说明](readme/xiaoqu5.jpg)
![输入图片说明](readme/xiaoqu6.png)
---

### 🔔 使用须知
1.允许用于个人学习、毕业设计、教学案例、公益事业、商业使用;<br>
2.如果商用必须保留版权信息，请自觉遵守;<br>
3.禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。<br>

---
### 🪪 版权信息
本项目包含的第三方源码和二进制文件之版权信息另行标注。<br>
版权所有Copyright © 2017-2024 by 萤丰信息 (https://www.sdyingfeng.cn)<br>
All rights reserved。<br>
著作权所有者为山东萤丰信息技术有限公司。<br>

---





